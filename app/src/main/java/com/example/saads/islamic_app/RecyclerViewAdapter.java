package com.example.saads.islamic_app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Locale;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.RecyclerViewHolder> {

static class RecyclerViewHolder extends RecyclerView.ViewHolder {

    private TextView name, number, emailID;
    public RelativeLayout row_item;
    private final Context context;

    RecyclerViewHolder(View view) {
        super(view);
        context = itemView.getContext();
        name = view.findViewById(R.id.surah);
        number = view.findViewById(R.id.num);
        row_item=itemView.findViewById(R.id.parent_bg);
       // emailID = view.findViewById(R.id.email_label);
    }

}

    private ArrayList<Data> arrayList;
    private ArrayList<Data> filterArrayList;//duplicate list for filtering
    private Context context;


    public RecyclerViewAdapter(Context context, ArrayList<Data> arrayList) {
        this.arrayList = arrayList;
        this.context = context;

        this.filterArrayList = new ArrayList<>();//initiate filter list
        this.filterArrayList.addAll(arrayList);//add all items of array list to filter list
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_item, viewGroup, false);
        return new RecyclerViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {

        Data model = arrayList.get(position);
        holder.name.setText(model.getDescription());
        holder.number.setText(model.getImgId());


        holder.row_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // String URL = data.get(position).geturl();

                //Toast.makeText(getApplicationContext(),"URL:"+URL,Toast.LENGTH_LONG).show();
                String num =holder.number.getText().toString();


                String nam = holder.name.getText().toString();

                Intent intent= new Intent(context,Ayat.class);
                //intent.putExtra("list", data);
                intent.putExtra("index",num );
                intent.putExtra("name", nam);
                // intent.putExtra("url",URL);
                context.startActivity(intent);

            }
        });





        //holder.emailID.setText(model.getEmailID());
    }


    @Override
    public int getItemCount() {
        return (null != arrayList ? arrayList.size() : 0);
    }


    // Filter Class to filter data
    public void filter(FilterType filterType, String charText, boolean isSearchWithPrefix) {

        //If Filter type is NAME and EMAIL then only do lowercase, else in case of NUMBER no need to do lowercase because of number format
        if (filterType == FilterType.NAME)// || filterType == FilterType.EMAIL)
            charText = charText.toLowerCase(Locale.getDefault());

        arrayList.clear();//Clear the main ArrayList

        //If search query is null or length is 0 then add all filterList items back to arrayList
        if (charText.length() == 0) {
            arrayList.addAll(filterArrayList);
        } else {

            //Else if search query is not null do a loop to all filterList items
            for (Data model : filterArrayList) {

                //Now check the type of search filter
                switch (filterType) {
                    case NAME:
//                        if (isSearchWithPrefix) {
                        //if STARTS WITH radio button is selected then it will match the exact NAME which match with search query
//                        if (model.getDescription().toLowerCase(Locale.getDefault()).startsWith(charText))
//                            arrayList.add(model);
////                        } else {
                            //if CONTAINS radio button is selected then it will match the NAME wherever it contains search query
                            if (model.getDescription().toLowerCase(Locale.getDefault()).contains(charText))
                                arrayList.add(model);
//                        }

                        break;
//                    case EMAIL:
//                        if (isSearchWithPrefix) {
//                            //if STARTS WITH radio button is selected then it will match the exact EMAIL which match with search query
//                            if (model.getEmailID().toLowerCase(Locale.getDefault()).startsWith(charText))
//                                arrayList.add(model);
//                        } else {
//                            //if CONTAINS radio button is selected then it will match the EMAIL wherever it contains search query
//                            if (model.getEmailID().toLowerCase(Locale.getDefault()).contains(charText))
//                                arrayList.add(model);
//                        }

//                        break;
                    case NUMBER:
                        // if (isSearchWithPrefix) {
                        //if STARTS WITH radio button is selected then it will match the exact NUMBER which match with search query
                        if (model.getImgId().startsWith(charText))
                            arrayList.add(model);
//                        } else {
//                            //if CONTAINS radio button is selected then it will match the NUMBER wherever it contains search query
//                            if (model.getNumber().contains(charText))
//                                arrayList.add(model);
//                        }
////
                        break;
                }

            }
        }
        notifyDataSetChanged();
    }

}
