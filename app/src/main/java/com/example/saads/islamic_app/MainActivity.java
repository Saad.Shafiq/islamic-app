package com.example.saads.islamic_app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static android.widget.Toast.LENGTH_LONG;

public class MainActivity extends Activity {
        private Context context;
        private RecyclerViewAdapter adapter;
        private ArrayList<Data> arrayList;
        private EditText searchEditText;


        /*  Filter Type to identify the type of Filter  */
        FilterType filterType;

        /*  boolean variable for Filtering */
        private boolean isSearchWithPrefix = false;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);

             filterType = FilterType.NAME;
            //filterType = FilterType.NUMBER;


            searchEditText = findViewById(R.id.search_text);



            RecyclerView recyclerView = findViewById(R.id.recycler_view);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
            recyclerView.setLayoutManager(linearLayoutManager);
            arrayList = fill_with_data();
            adapter = new RecyclerViewAdapter(this, arrayList);
            recyclerView.setAdapter(adapter);


            searchEditText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    //On text changed in Edit text start filtering the list
                   // filterType = FilterType.NUMBER;
                     filterType = FilterType.NAME;
                    adapter.filter(filterType, charSequence.toString(), isSearchWithPrefix);

                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });
        }







    public ArrayList<Data> fill_with_data()
    {
        ArrayList<Data> data = new ArrayList<>();

        data.add(new Data("1", "Al-Fatihah"));
        data.add(new Data("2", "Al-Baqarah"));
        data.add(new Data("3", "Aali Imran"));
        data.add(new Data("4", "An-Nisa"));
        data.add(new Data("5", "Al-Ma’idah"));
        data.add(new Data("6", "Al-An’am"));
        data.add(new Data("7", "Al-A’raf"));
        data.add(new Data("8", "Al-Anfal"));
        data.add(new Data("9", "At-Taubah"));
        data.add(new Data("10","Yunus"));
        data.add(new Data("11","Hud"));
        data.add(new Data("12","Yusuf"));
        data.add(new Data("13"," Ar-Ra’d"));
        data.add(new Data("14"," Ibrahim"));
        data.add(new Data("15"," Al-Hijr"));
        data.add(new Data("16"," An-Nahl"));
        data.add(new Data("17"," Al-Isra"));
        data.add(new Data("18"," Al-Kahf"));
        data.add(new Data("19"," Maryam"));
        data.add(new Data("20"," Ta-Ha"));
        data.add(new Data("21"," Al-Anbiya"));
        data.add(new Data("22"," Al-Haj the"));
        data.add(new Data("23"," Al-Mu’minun"));
        data.add(new Data("24"," An-Nur"));
        data.add(new Data("25"," Al-Furqan"));
        data.add(new Data("26"," Ash-Shu’ara"));
        data.add(new Data("27"," An-Naml"));
        data.add(new Data("28"," Al-Qasas"));
        data.add(new Data("29"," Al-Ankabut"));
        data.add(new Data("30"," Ar-Rum"));
        data.add(new Data("31"," Luqman"));
        data.add(new Data("32"," As-Sajdah"));
        data.add(new Data("33"," Al-Ahzab"));
        data.add(new Data("34"," Saba"));
        data.add(new Data("35"," Al-Fatir"));
        data.add(new Data("36"," Ya-Sin"));
        data.add(new Data("37"," As-Saffah"));
        data.add(new Data("38"," Sad"));
        data.add(new Data("39"," Az-Zumar"));
        data.add(new Data("40"," Ghafar"));
        data.add(new Data("41"," Fusilat"));
        data.add(new Data("42"," Ash-Shura"));
        data.add(new Data("43"," Az-Zukhruf"));
        data.add(new Data("44"," Ad-Dukhan"));
        data.add(new Data("45"," Al-Jathiyah"));
        data.add(new Data("46"," Al-Ahqaf "));
        data.add(new Data("47"," Muhammad"));
        data.add(new Data("48"," Al-Fat’h"));
        data.add(new Data("49"," Al-Hujurat "));
        data.add(new Data("50"," Qaf"));
        data.add(new Data("51"," Adz-Dzariyah "));
        data.add(new Data("52"," At-Tur"));
        data.add(new Data("53"," An-Najm"));
        data.add(new Data("54"," Al-Qamar"));
        data.add(new Data("55"," Ar-Rahman "));
        data.add(new Data("56"," Al-Waqi’ah"));
        data.add(new Data("57"," Al-Hadid "));
        data.add(new Data("58"," Al-Mujadilah"));
        data.add(new Data("59"," Al-Hashr"));
        data.add(new Data("60"," Al-Mumtahanah"));
        data.add(new Data("61"," As-Saf"));
        data.add(new Data("62"," Al-Jum’ah"));
        data.add(new Data("63"," Al-Munafiqun"));
        data.add(new Data("64"," At-Taghabun"));
        data.add(new Data("65"," At-Talaq"));
        data.add(new Data("66"," At-Tahrim"));
        data.add(new Data("67"," Al-Mulk "));
        data.add(new Data("68"," Al-Qalam "));
        data.add(new Data("69"," Al-Haqqah"));
        data.add(new Data("70"," Al-Ma’arij"));
        data.add(new Data("71"," Nuh "));
        data.add(new Data("72"," Al-Jinn"));
        data.add(new Data("73"," Al-Muzammil"));
        data.add(new Data("74"," Al-Mudaththir"));
        data.add(new Data("75"," Al-Qiyamah"));
        data.add(new Data("76"," Al-Insan"));
        data.add(new Data("77"," Al-Mursalat"));
        data.add(new Data("78"," An-Naba "));
        data.add(new Data("79"," An-Nazi"));
        data.add(new Data("80"," ‘Abasa"));
        data.add(new Data("81"," At-Takwir"));
        data.add(new Data("82"," Al-Infitar"));
        data.add(new Data("83"," Al-Mutaffifin"));
        data.add(new Data("84"," Al-Inshiqaq"));
        data.add(new Data("85"," Al-Buruj"));
        data.add(new Data("86"," At-Tariq "));
        data.add(new Data("87"," Al-A’la"));
        data.add(new Data("88"," Al-Ghashiyah"));
        data.add(new Data("89"," Al-Fajr"));
        data.add(new Data("90"," Al-Balad"));
        data.add(new Data("91"," Ash-Shams"));
        data.add(new Data("92"," Al-Layl"));
        data.add(new Data("93"," Adh-Dhuha"));
        data.add(new Data("94"," Al-Inshirah"));
        data.add(new Data("95"," At-Tin"));
        data.add(new Data("96"," Al-‘Alaq"));
        data.add(new Data("97"," Al-Qadar"));
        data.add(new Data("98"," Al-Bayinah"));
        data.add(new Data("99"," Az-Zalzalah"));
        data.add(new Data("100"," Al-‘Adiyah"));
        data.add(new Data("101"," Al-Qari’ah"));
        data.add(new Data("102"," At-Takathur"));
        data.add(new Data("103"," Al-‘Asr"));
        data.add(new Data("104"," Al-Humazah"));
        data.add(new Data("105"," Al-Fil"));
        data.add(new Data("106"," Quraish"));
        data.add(new Data("107"," Al-Ma’un"));
        data.add(new Data("108"," Al-Kauthar"));
        data.add(new Data("109"," Al-Kafirun"));
        data.add(new Data("110"," An-Nasr"));
        data.add(new Data("111"," Al-Masad "));
        data.add(new Data("112"," Al-Ikhlas"));
        data.add(new Data("113"," Al-Falaq"));
        data.add(new Data("114"," An-Nas"));


        return data;
    }


}
