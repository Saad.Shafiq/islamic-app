package com.example.saads.islamic_app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class Ayat extends  Activity {
    private Context context;
    private Ayat_Adapter adapter;
    private ArrayList<Number> arrayList;
    private EditText searchEditText;
    private ImageView back_btn;
    private TextView surah;
    ProgressBar loader;
    String index;
    private int one=7,two=286,three=70,four=80,five=90,six=100,seven=100,eight=70,nine=80,ten=30;


    /*  Filter Type to identify the type of Filter  */
    FilterType filterType;

    /*  boolean variable for Filtering */
    private boolean isSearchWithPrefix = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ayat);

       // index = getIntent().getIntExtra("index", 0);
        //Toast.makeText(getApplicationContext(), "NEW" + index, Toast.LENGTH_LONG).show();

        index =getIntent().getStringExtra("index");

        String surah_name = getIntent().getStringExtra("name");

        // Toast.makeText(getApplicationContext(), index, Toast.LENGTH_LONG).show();

        back_btn = findViewById(R.id.back);
        // searchEditText = findViewById(R.id.search_text);
        searchEditText = findViewById(R.id.search_text);
        loader = findViewById(R.id.ayat_progressBar);
        loader.setVisibility(View.GONE);
        searchEditText.setHint("   Search Ayat  ");

        surah = findViewById(R.id.surah_heading);

        surah.setText(surah_name);
        back_btn.setVisibility(View.VISIBLE);

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();

//                Intent intent = new Intent(Ayat.this, MainActivity.class);
//                startActivity(intent);
//                finish();
            }
        });

        //filterType = FilterType.NAME;
        filterType = FilterType.NUMBER;


        searchEditText = findViewById(R.id.search_text);


        RecyclerView recyclerView = findViewById(R.id.ayat_recycler_view);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(linearLayoutManager);

        searchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                System.out.print("Gone");
                //On text changed in Edit text start filtering the list
               // filterType = FilterType.NUMBER;
                // filterType = FilterType.NAME;
                adapter.filter(filterType, charSequence.toString(), isSearchWithPrefix);

            }

            @Override
            public void afterTextChanged(Editable editable) {
               System.out.print("DONE");
            }
        });

        arrayList = fill_with_data();

        adapter = new Ayat_Adapter(this, arrayList, loader);
        recyclerView.setAdapter(adapter);



    }

    public ArrayList<Number> fill_with_data()
    {
        ArrayList<Number> data = new ArrayList<>();


        switch (index)
        {
            case "1":

                for(int i=1;i<=7;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;

            case "2":

                for(int i=1;i<=286;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;

            case "3":
                for(int i=1;i<=200;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;

            case "4":
                for(int i=1;i<=176;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "5":
                for(int i=1;i<=120;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "6":
                for(int i=1;i<=165;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "7":
                for(int i=1;i<=206;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "8":
                for(int i=1;i<=75;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "9":
                for(int i=1;i<=129;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "10":
                for(int i=1;i<=109;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "11":

                for(int i=1;i<=123;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;

            case "12":
                for(int i=1;i<=111;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;

            case "13":
                for(int i=1;i<=43;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "14":
                for(int i=1;i<=52;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "15":
                for(int i=1;i<=99;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "16":
                for(int i=1;i<=128;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "17":
                for(int i=1;i<=111;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "18":
                for(int i=1;i<=110;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "19":
                for(int i=1;i<=98;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "20":

                for(int i=1;i<=135;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;

            case "21":
                for(int i=1;i<=112;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;

            case "22":
                for(int i=1;i<=78;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "23":
                for(int i=1;i<=118;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "24":
                for(int i=1;i<=64;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "25":
                for(int i=1;i<=77;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "26":
                for(int i=1;i<=227;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "27":
                for(int i=1;i<=93;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "28":
                for(int i=1;i<=88;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "29":

                for(int i=1;i<=69;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;

            case "30":
                for(int i=1;i<=60;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;

            case "31":
                for(int i=1;i<=34;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "32":
                for(int i=1;i<=30;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "33":
                for(int i=1;i<=73;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "34":
                for(int i=1;i<=54;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "35":
                for(int i=1;i<=45;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "36":
                for(int i=1;i<=83;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "37":
                for(int i=1;i<=182;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "38":

                for(int i=1;i<=88;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;

            case "39":
                for(int i=1;i<=75;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;

            case "40":
                for(int i=1;i<=85;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "41":
                for(int i=1;i<=54;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "42":
                for(int i=1;i<=53;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "43":
                for(int i=1;i<=89;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "44":
                for(int i=1;i<=59;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "45":
                for(int i=1;i<=37;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "46":
                for(int i=1;i<=35;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "47":

                for(int i=1;i<=38;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;

            case "48":
                for(int i=1;i<=29;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;

            case "49":
                for(int i=1;i<=18;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "50":
                for(int i=1;i<=45;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "51":
                for(int i=1;i<=60;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "52":
                for(int i=1;i<=49;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "53":
                for(int i=1;i<=62;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "54":
                for(int i=1;i<=55;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "55":
                for(int i=1;i<=78;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "56":

                for(int i=1;i<=96;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;

            case "57":
                for(int i=1;i<=29;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;

            case "58":
                for(int i=1;i<=22;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "59":
                for(int i=1;i<=24;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "60":
                for(int i=1;i<=13;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "61":
                for(int i=1;i<=14;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "62":
                for(int i=1;i<=11;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "63":
                for(int i=1;i<=11;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "64":
                for(int i=1;i<=18;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;


            case "65":

                for(int i=1;i<=12;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;

            case "66":
                for(int i=1;i<=12;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;

            case "67":
                for(int i=1;i<=30;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "68":
                for(int i=1;i<=52;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "69":
                for(int i=1;i<=52;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "70":
                for(int i=1;i<=44;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "71":
                for(int i=1;i<=28;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "72":
                for(int i=1;i<=28;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "73":
                for(int i=1;i<=20;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "74":

                for(int i=1;i<=56;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;

            case "75":
                for(int i=1;i<=40;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;

            case "76":
                for(int i=1;i<=31;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "77":
                for(int i=1;i<=50;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "78":
                for(int i=1;i<=40;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "79":
                for(int i=1;i<=46;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "80":
                for(int i=1;i<=42;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "81":
                for(int i=1;i<=29;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "82":
                for(int i=1;i<=19;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "83":

                for(int i=1;i<=36;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;

            case "84":
                for(int i=1;i<=25;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;

            case "85":
                for(int i=1;i<=22;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "86":
                for(int i=1;i<=17;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "87":
                for(int i=1;i<=19;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "88":
                for(int i=1;i<=26;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "89":
                for(int i=1;i<=30;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "90":
                for(int i=1;i<=20;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "91":
                for(int i=1;i<=15;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "92":

                for(int i=1;i<=21;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;

            case "93":
                for(int i=1;i<=11;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;

            case "94":
                for(int i=1;i<=8;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "95":
                for(int i=1;i<=8;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "96":
                for(int i=1;i<=19;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "97":
                for(int i=1;i<=5;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "98":
                for(int i=1;i<=8;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "99":
                for(int i=1;i<=8;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "100":
                for(int i=1;i<=11;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "101":

                for(int i=1;i<=11;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;

            case "102":
                for(int i=1;i<=8;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;

            case "103":
                for(int i=1;i<=3;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "104":
                for(int i=1;i<=9;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "105":
                for(int i=1;i<=5;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "106":
                for(int i=1;i<=4;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "107":
                for(int i=1;i<=7;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "108":
                for(int i=1;i<=3;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "109":
                for(int i=1;i<=6;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "110":

                for(int i=1;i<=3;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;

            case "111":
                for(int i=1;i<=5;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;

            case "112":
                for(int i=1;i<=4;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "113":
                for(int i=1;i<=5;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;
            case "114":
                for(int i=1;i<=6;i++)
                {
                    String numberAsString = Integer.toString(i);
                    data.add(new Number(numberAsString));
                }

                break;

        }


        return data;
    }
}

//    public ArrayList<Data> fill_with_data()
//    {
//        ArrayList<Data> data = new ArrayList<>();
//
//        data.add(new Data("1", "Al-Fatihah"));
//        data.add(new Data("2", "Al-Baqarah"));
//        data.add(new Data("3", "Aali Imran"));
//        data.add(new Data("4", "An-Nisa"));
//        data.add(new Data("5", "Al-Ma’idah"));
//        data.add(new Data("6", "Al-An’am"));
//        data.add(new Data("7", "Al-A’raf"));
//        data.add(new Data("8", "Al-Anfal"));
//        data.add(new Data("9", "At-Taubah"));
//        data.add(new Data("10","Yunus"));
//        data.add(new Data("11","Hud"));
//        data.add(new Data("12","Yusuf"));
//        data.add(new Data("13"," Ar-Ra’d"));
//        data.add(new Data("14"," Ibrahim"));
//        data.add(new Data("15"," Al-Hijr"));
//        data.add(new Data("16"," An-Nahl"));
//        data.add(new Data("17"," Al-Isra"));
//        data.add(new Data("18"," Al-Kahf"));
//        data.add(new Data("19"," Maryam"));
//        data.add(new Data("20"," Ta-Ha"));
//        data.add(new Data("21"," Al-Anbiya"));
//        data.add(new Data("22"," Al-Haj the"));
//        data.add(new Data("23"," Al-Mu’minun"));
//        data.add(new Data("24"," An-Nur"));
//        data.add(new Data("25"," Al-Furqan"));
//        data.add(new Data("26"," Ash-Shu’ara"));
//        data.add(new Data("27"," An-Naml"));
//        data.add(new Data("28"," Al-Qasas"));
//        data.add(new Data("29"," Al-Ankabut"));
//        data.add(new Data("30"," Ar-Rum"));
//        data.add(new Data("31"," Luqman"));
//        data.add(new Data("32"," As-Sajdah"));
//        data.add(new Data("33"," Al-Ahzab"));
//        data.add(new Data("34"," Saba"));
//        data.add(new Data("35"," Al-Fatir"));
//        data.add(new Data("36"," Ya-Sin"));
//        data.add(new Data("37"," As-Saffah"));
//        data.add(new Data("38"," Sad"));
//        data.add(new Data("39"," Az-Zumar"));
//        data.add(new Data("40"," Ghafar"));
//        data.add(new Data("41"," Fusilat"));
//        data.add(new Data("42"," Ash-Shura"));
//        data.add(new Data("43"," Az-Zukhruf"));
//        data.add(new Data("44"," Ad-Dukhan"));
//        data.add(new Data("45"," Al-Jathiyah"));
//        data.add(new Data("46"," Al-Ahqaf "));
//        data.add(new Data("47"," Muhammad"));
//        data.add(new Data("48"," Al-Fat’h"));
//        data.add(new Data("49"," Al-Hujurat "));
//        data.add(new Data("50"," Qaf"));
//        data.add(new Data("51"," Adz-Dzariyah "));
//        data.add(new Data("52"," At-Tur"));
//        data.add(new Data("53"," An-Najm"));
//        data.add(new Data("54"," Al-Qamar"));
//        data.add(new Data("55"," Ar-Rahman "));
//        data.add(new Data("56"," Al-Waqi’ah"));
//        data.add(new Data("57"," Al-Hadid "));
//        data.add(new Data("58"," Al-Mujadilah"));
//        data.add(new Data("59"," Al-Hashr"));
//        data.add(new Data("60"," Al-Mumtahanah"));
//        data.add(new Data("61"," As-Saf"));
//        data.add(new Data("62"," Al-Jum’ah"));
//        data.add(new Data("63"," Al-Munafiqun"));
//        data.add(new Data("64"," At-Taghabun"));
//        data.add(new Data("65"," At-Talaq"));
//        data.add(new Data("66"," At-Tahrim"));
//        data.add(new Data("67"," Al-Mulk "));
//        data.add(new Data("68"," Al-Qalam "));
//        data.add(new Data("69"," Al-Haqqah"));
//        data.add(new Data("70"," Al-Ma’arij"));
//        data.add(new Data("71"," Nuh "));
//        data.add(new Data("72"," Al-Jinn"));
//        data.add(new Data("73"," Al-Muzammil"));
//        data.add(new Data("74"," Al-Mudaththir"));
//        data.add(new Data("75"," Al-Qiyamah"));
//        data.add(new Data("76"," Al-Insan"));
//        data.add(new Data("77"," Al-Mursalat"));
//        data.add(new Data("78"," An-Naba "));
//        data.add(new Data("79"," An-Nazi"));
//        data.add(new Data("80"," ‘Abasa"));
//        data.add(new Data("81"," At-Takwir"));
//        data.add(new Data("82"," Al-Infitar"));
//        data.add(new Data("83"," Al-Mutaffifin"));
//        data.add(new Data("84"," Al-Inshiqaq"));
//        data.add(new Data("85"," Al-Buruj"));
//        data.add(new Data("86"," At-Tariq "));
//        data.add(new Data("87"," Al-A’la"));
//        data.add(new Data("88"," Al-Ghashiyah"));
//        data.add(new Data("89"," Al-Fajr"));
//        data.add(new Data("90"," Al-Balad"));
//        data.add(new Data("91"," Ash-Shams"));
//        data.add(new Data("92"," Al-Layl"));
//        data.add(new Data("93"," Adh-Dhuha"));
//        data.add(new Data("94"," Al-Inshirah"));
//        data.add(new Data("95"," At-Tin"));
//        data.add(new Data("96"," Al-‘Alaq"));
//        data.add(new Data("97"," Al-Qadar"));
//        data.add(new Data("98"," Al-Bayinah"));
//        data.add(new Data("99"," Az-Zalzalah"));
//        data.add(new Data("100"," Al-‘Adiyah"));
//        data.add(new Data("101"," Al-Qari’ah"));
//        data.add(new Data("102"," At-Takathur"));
//        data.add(new Data("103"," Al-‘Asr"));
//        data.add(new Data("104"," Al-Humazah"));
//        data.add(new Data("105"," Al-Fil"));
//        data.add(new Data("106"," Quraish"));
//        data.add(new Data("107"," Al-Ma’un"));
//        data.add(new Data("108"," Al-Kauthar"));
//        data.add(new Data("109"," Al-Kafirun"));
//        data.add(new Data("110"," An-Nasr"));
//        data.add(new Data("111"," Al-Masad "));
//        data.add(new Data("112"," Al-Ikhlas"));
//        data.add(new Data("113"," Al-Falaq"));
//        data.add(new Data("114"," An-Nas"));
//
//
//        return data;
//    }
//
