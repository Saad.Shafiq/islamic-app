package com.example.saads.islamic_app;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class Audio_Playing extends Activity {


    ImageView back_btn;
    private Button btnPlay;
    private Button btnPouse;
    private boolean playPause;
    private Button Share;
    private Button Download;
    private MediaPlayer mPlayer;
    private ProgressDialog progressDialog;
    private boolean initialStage = true;
    SeekBar mSeekBarPlayer;
    private int current = 0;
    private boolean running = true;
    private int duration = 0;
    private TextView mMediaTime;
    ProgressBar loader;
    Uri uri;
    boolean mCancel;
    String URL;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_audio_playing);

        loader = findViewById(R.id.progressBar);
        loader.setVisibility(View.VISIBLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

        int index = getIntent().getIntExtra("index", 0);

        final String surah_name = getIntent().getStringExtra("name");
//        ArrayList<Data> list;
//        list = (ArrayList<Data>) getIntent().getSerializableExtra("list");


        //Toast.makeText(getApplicationContext(), "NEW" + index, Toast.LENGTH_LONG).show();

        //  String URL = getIntent().getStringExtra("url");
        //Toast.makeText(getApplicationContext(),"URL:"+URL,Toast.LENGTH_LONG).show();
        //Toast.makeText(this,""+list,Toast.LENGTH_LONG).show();

        //final String URL="http://138.68.229.113/islamic_videos/audio1.mp3";

        back_btn = findViewById(R.id.back);
        back_btn.setVisibility(View.VISIBLE);



        Share = findViewById(R.id.Share);
        Download = findViewById(R.id.download_btn);

        Download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mPlayer.pause();

                Intent intent = new Intent(Audio_Playing.this, Download_url.class);

                intent.putExtra("URL",URL );
                // intent.putExtra("url",URL);
                startActivity(intent);
                btnPlay.setVisibility(View.VISIBLE);
                btnPouse.setVisibility(View.GONE);

            }
        });


        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                cancel();
                mPlayer.stop();
                mPlayer.pause();
                onBackPressed();
                finish();
//                Intent intent = new Intent(Audio_Playing.this, Ayat.class);
//                intent.putExtra("name",surah_name);
//                startActivity(intent);

            }
        });

        Share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mPlayer.pause();
                try {
                    Intent i = new Intent(Intent.ACTION_SEND);
                    i.setType("text/plain");
                    String sAux = "\nLet me recommend you this! \n\n";
                    sAux = sAux + "http://138.68.229.113/islamic_videos/001-001";
                    i.putExtra(Intent.EXTRA_TEXT, sAux);
                    startActivity(Intent.createChooser(i, "choose one"));
                } catch (Exception e) {
                    //e.toString();
                }
                btnPlay.setVisibility(View.VISIBLE);
                btnPouse.setVisibility(View.GONE);


            }
        });



        //Uri uri = Uri.parse("http://138.68.229.113/islamic_videos/audio.mp3");


        if (index == 0) {
            URL="http://138.68.229.113/islamic_videos/audio1.mp3";

            //uri = Uri.parse("http://138.68.229.113/islamic_videos/audio1.mp3");


        } else if (index == 1) {
            URL="http://138.68.229.113/islamic_videos/audio2.mp3";
           // uri = Uri.parse("http://138.68.229.113/islamic_videos/audio2");


        } else if (index == 2) {
            URL="http://138.68.229.113/islamic_videos/audio3.mp3";
           // uri = Uri.parse("http://138.68.229.113/islamic_videos/audio3");


        } else {
            URL="http://138.68.229.113/islamic_videos/audio3.mp3";
           // uri = Uri.parse("http://138.68.229.113/islamic_videos/audio3");

        }

        btnPlay = findViewById(R.id.play);
        btnPouse = findViewById(R.id.pause);
        mMediaTime = findViewById(R.id.media_time);
        mSeekBarPlayer = findViewById(R.id.seekbar);

       // mPlayer = MediaPlayer.create(Audio_Playing.this, uri);

       // mPlayer.setOnPreparedListener(this);

        mPlayer = new MediaPlayer();
        mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);

        try {

            mPlayer.setDataSource(URL);
            mPlayer.prepareAsync();
            mPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {

                    if(mCancel){
                        mPlayer.release();
                        //nullify your MediaPlayer reference
                        mPlayer = null;
                    }
                    else{
                        mPlayer.start();

                        duration = mPlayer.getDuration();
                        mSeekBarPlayer.setMax(duration);
                        mSeekBarPlayer.postDelayed(onEverySecond, 1000);
                        btnPouse.setVisibility(View.VISIBLE);
                        btnPlay.setVisibility(View.GONE);
                        loader.setVisibility(View.GONE);
                        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                    }



                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }






        mSeekBarPlayer.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    mPlayer.seekTo(progress);
                    updateTime();
                }
            }
        });

        btnPlay.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
//                try {
//                    mPlayer.prepare();
//                } catch (IllegalStateException e) {
//                    // TODO Auto-generated catch block
//                    e.printStackTrace();
//                } catch (IOException e) {
//                    // TODO Auto-generated catch block
//                    e.printStackTrace();
//                }

                if (mPlayer.isPlaying()) {
                    mPlayer.pause();
                } else {
                    mPlayer.start();

                    duration = mPlayer.getDuration();
                    mSeekBarPlayer.setMax(duration);
                    mSeekBarPlayer.postDelayed(onEverySecond, 1000);
                    btnPouse.setVisibility(View.VISIBLE);
                    btnPlay.setVisibility(View.GONE);

                }

            }
        });


        btnPouse.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                mPlayer.pause();
                btnPouse.setVisibility(View.GONE);
                btnPlay.setVisibility(View.VISIBLE);
            }
        });
    }


    private Runnable onEverySecond = new Runnable() {
        @Override
        public void run() {
            if (true == running) {
                if (mSeekBarPlayer != null) {
                    mSeekBarPlayer.setProgress(mPlayer.getCurrentPosition());
                }

                if (mPlayer.isPlaying()) {
                    mSeekBarPlayer.postDelayed(onEverySecond, 1000);
                    updateTime();
                }
            }
        }
    };

    private void updateTime() {
        do {
            current = mPlayer.getCurrentPosition();
            System.out.println("duration - " + duration + " current- "
                    + current);
            int dSeconds = (int) (duration / 1000) % 60;
            int dMinutes = (int) ((duration / (1000 * 60)) % 60);
            int dHours = (int) ((duration / (1000 * 60 * 60)) % 24);

            int cSeconds = (int) (current / 1000) % 60;
            int cMinutes = (int) ((current / (1000 * 60)) % 60);
            int cHours = (int) ((current / (1000 * 60 * 60)) % 24);

            if (dHours == 0) {
                mMediaTime.setText(String.format("%02d:%02d / %02d:%02d", cMinutes, cSeconds, dMinutes, dSeconds));
            } else {
                mMediaTime.setText(String.format("%02d:%02d:%02d / %02d:%02d:%02d", cHours, cMinutes, cSeconds, dHours, dMinutes, dSeconds));
            }

            try {
                Log.d("Value: ", String.valueOf((int) (current * 100 / duration)));
                if (mSeekBarPlayer.getProgress() >= 100) {
                    break;
                }
            } catch (Exception e) {
            }
        } while (mSeekBarPlayer.getProgress() <= 100);
    }
    public void cancel(){
        mCancel = true;
    }




//    @Override
//    public void onPrepared(MediaPlayer arg0) {
//        // TODO Auto-generated method stub
//        duration = mPlayer.getDuration();
//        mSeekBarPlayer.setMax(duration);
//        mSeekBarPlayer.postDelayed(onEverySecond, 1000);
//        loader.setVisibility(View.GONE);
//    }
}