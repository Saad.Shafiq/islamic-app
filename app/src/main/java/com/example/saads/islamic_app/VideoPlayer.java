package com.example.saads.islamic_app;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

public class VideoPlayer extends Activity {


    ImageView back_btn;
    private Button play_btn;
    private Button btnPouse;
    private boolean playPause;
    private Button Share;
    private Button Download;
    private MediaPlayer mediaPlayer;
    private ProgressDialog progressDialog;
    private boolean initialStage = true;
    SeekBar mSeekBar;
    private int current = 0;
    private boolean running = true;
    private int duration = 0;
    private TextView mMediaTime;

    private String url;
    private Uri vidUri;
    ProgressBar loader;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_player);

         loader=findViewById(R.id.video_progress);
        loader.setVisibility(View.VISIBLE);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

        final VideoView vidView =findViewById(R.id.myVideo);
        MediaController vidControl = new MediaController(this);

        //url = getIntent().getStringExtra("URL");

//        ArrayList<Data> list;
//        list = (ArrayList<Data>) getIntent().getSerializableExtra("list");


        int index = getIntent().getIntExtra("index", 0);
       // Toast.makeText(getApplicationContext(), "NEW" + index, Toast.LENGTH_LONG).show();

        //  String URL = getIntent().getStringExtra("url");
        //Toast.makeText(getApplicationContext(),"URL:"+URL,Toast.LENGTH_LONG).show();
        //Toast.makeText(this,""+list,Toast.LENGTH_LONG).show();

       // final String video_URL="http://138.68.229.113/islamic_videos/001-001.mp4";

        //String vidAddress = URL;

        if (index == 0) {

            vidUri = Uri.parse("http://138.68.229.113/islamic_videos/001-001.mp4");


        } else if (index == 1) {
            // Toast.makeText(this, "NOT SET", Toast.LENGTH_LONG).show();
            vidUri = Uri.parse("http://138.68.229.113/islamic_videos/001-002.mp4");


        } else if (index == 2) {
            // Toast.makeText(this, "NOT SET", Toast.LENGTH_LONG).show();
            vidUri = Uri.parse("http://138.68.229.113/islamic_videos/001-003.mp4");


        } else {
            // Toast.makeText(this, "NOT SET", Toast.LENGTH_LONG).show();
            vidUri = Uri.parse("http://138.68.229.113/islamic_videos/001-004.mp4");

        }


       // final   Uri vidUri = Uri.parse(uri);

        back_btn = findViewById(R.id.back);
        back_btn.setVisibility(View.VISIBLE);

        Share = findViewById(R.id.Share);
        Download= findViewById(R.id.download_btn);

//        play_btn = findViewById(R.id.play);
//        btnPouse = findViewById(R.id.pause);


        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                vidView.stopPlayback();
                // mediaPlayer.pause();

               onBackPressed();
               finish();
            }
        });

        Share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                vidView.pause();
                try {
                    Intent i = new Intent(Intent.ACTION_SEND);
                    i.setType("text/plain");
                    String sAux = "\nLet me recommend you this! \n\n";
                    sAux = sAux + "http://138.68.229.113/islamic_videos/001-001";
                    i.putExtra(Intent.EXTRA_TEXT, sAux);
                    startActivity(Intent.createChooser(i, "choose one"));
                } catch(Exception e) {
                    //e.toString();
                }
            }
        });



        vidView.setVideoURI(vidUri);

        vidControl.setAnchorView(vidView);
        vidView.setMediaController(vidControl);
        vidView.setOnPreparedListener(
                new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mediaPlayer) {
                        loader.setVisibility(View.GONE);
                        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);


                        vidView.start();
                    }
                });

        Download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                vidView.pause();
                Intent intent = new Intent(VideoPlayer.this, Download_url.class);
                String url=vidUri.toString();

                intent.putExtra("URL",url );
               // Toast.makeText(getApplicationContext(), "URL" + vidUri, Toast.LENGTH_LONG).show();
                // intent.putExtra("url",URL);
                startActivity(intent);

            }
        });


    }

    @Override
    protected void onStart() {
        super.onStart();
        loader.setVisibility(View.VISIBLE);

    }
}

