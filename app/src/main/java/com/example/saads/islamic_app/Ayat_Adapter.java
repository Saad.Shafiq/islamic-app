package com.example.saads.islamic_app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Locale;

public class Ayat_Adapter extends RecyclerView.Adapter<Ayat_Adapter.RecyclerViewHolder> {

    static class RecyclerViewHolder extends RecyclerView.ViewHolder {

        private TextView name, ayat_number, emailID;
        public RelativeLayout row_item;
        private final Context context;
        private ImageView mp3,mp4;
        ProgressBar loader;

        RecyclerViewHolder(View view) {
            super(view);
            context = itemView.getContext();
            ayat_number = view.findViewById(R.id.ayat_num);
            row_item=view.findViewById(R.id.ayat_parent);
            // emailID = view.findViewById(R.id.email_label);
            mp3=view.findViewById(R.id.mp3_image);
            mp4=view.findViewById(R.id.mp4_image);
            loader=view.findViewById(R.id.ayat_progressBar);

        }

    }

    private ArrayList<Number> arrayList;
    private ArrayList<Number> filterArrayList;//duplicate list for filtering
    private Context context;
    private ProgressBar loader;


     Ayat_Adapter(Context context, ArrayList<Number> arrayList,ProgressBar loader) {
        this.arrayList = arrayList;
        this.context = context;
        this.loader=loader;
         loader.setVisibility(View.GONE);
        this.filterArrayList = new ArrayList<>();//initiate filter list

            this.filterArrayList.addAll(arrayList);//add all items of array list to filter list
    }

    @Override
    public Ayat_Adapter.RecyclerViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.ayat_row, viewGroup, false);
        return new Ayat_Adapter.RecyclerViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final Ayat_Adapter.RecyclerViewHolder holder, final int position) {


     final    Number model = arrayList.get(position);
        // holder.name.setText(model.getDescription());
        holder.ayat_number.setText(model.getId());


        holder.mp3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // String URL = data.get(position).geturl();

                //Toast.makeText(getApplicationContext(),"URL:"+URL,Toast.LENGTH_LONG).show();
          if(netConnect(context))
          {
              Intent intent= new Intent(context,Audio_Playing.class);
              //intent.putExtra("list", data);
              intent.putExtra("index", model);

              context.startActivity(intent);




          }
          else
          {
              Toast.makeText(context," Please Turn on Internet ",Toast.LENGTH_LONG).show();
          }





            }
        });
        holder.mp4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // String URL = data.get(position).geturl();

                //Toast.makeText(getApplicationContext(),"URL:"+URL,Toast.LENGTH_LONG).show();

                if(netConnect(context)) {

                    Intent intent = new Intent(context, VideoPlayer.class);
                    //intent.putExtra("list", data);
                    intent.putExtra("index", position);
                    // intent.putExtra("url",URL);
                    context.startActivity(intent);

                }
                else
                {
                    Toast.makeText(context," Please Turn on Internet ",Toast.LENGTH_LONG).show();
                }

            }
        });





        //holder.emailID.setText(model.getEmailID());
    }


    @Override
    public int getItemCount() {
        return (null != arrayList ? arrayList.size() : 0);
    }


    // Filter Class to filter data
    public void filter(FilterType filterType, String charText, boolean isSearchWithPrefix) {

        //If Filter type is NAME and EMAIL then only do lowercase, else in case of NUMBER no need to do lowercase because of number format
        if (filterType == FilterType.NAME)// || filterType == FilterType.EMAIL)
            charText = charText.toLowerCase(Locale.getDefault());

        arrayList.clear();//Clear the main ArrayList

        //If search query is null or length is 0 then add all filterList items back to arrayList
//        if (charText.length() == 0) {
//            arrayList.addAll(filterArrayList);
//        } else {
           arrayList.clear();

            //Else if search query is not null do a loop to all filterList items
            for (Number model : filterArrayList) {

                //Now check the type of search filter
                switch (filterType) {
                   // case NAME:
//                        if (isSearchWithPrefix) {
                        //if STARTS WITH radio button is selected then it will match the exact NAME which match with search query
//                        if (model.getDescription().toLowerCase(Locale.getDefault()).startsWith(charText))
//                            arrayList.add(model);
////                        } else {
                        //if CONTAINS radio button is selected then it will match the NAME wherever it contains search query
                     //   if (model.getDescription().toLowerCase(Locale.getDefault()).contains(charText))
                       //     arrayList.add(model);
//                        }

                       // break;
//                    case EMAIL:
//                        if (isSearchWithPrefix) {
//                            //if STARTS WITH radio button is selected then it will match the exact EMAIL which match with search query
//                            if (model.getEmailID().toLowerCase(Locale.getDefault()).startsWith(charText))
//                                arrayList.add(model);
//                        } else {
//                            //if CONTAINS radio button is selected then it will match the EMAIL wherever it contains search query
//                            if (model.getEmailID().toLowerCase(Locale.getDefault()).contains(charText))
//                                arrayList.add(model);
//                        }

//                        break;
                    case NUMBER:
                        // if (isSearchWithPrefix) {
                        //if STARTS WITH radio button is selected then it will match the exact NUMBER which match with search query
                        if (model.getId().startsWith(charText))
                            arrayList.add(model);
//                        } else {
//                            //if CONTAINS radio button is selected then it will match the NUMBER wherever it contains search query
//                            if (model.getNumber().contains(charText))
//                                arrayList.add(model);
//                        }
////
                        break;
                }

            }
       // }
        notifyDataSetChanged();
    }


    public boolean netConnect(Context ctx)
    {
        ConnectivityManager cm;
        NetworkInfo info = null;
        try
        {
            cm = (ConnectivityManager)
                    ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
            info = cm.getActiveNetworkInfo();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        if (info != null)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

  

}