package com.example.saads.islamic_app;

import java.io.Serializable;

public class Data implements Serializable {
    public String imageId;
    public String txt;

    Data(String imageId, String text) {

        this.imageId = imageId;
        this.txt=text;
    }

    public String getDescription() {
        return txt;
    }

    public void setDescription(String description) {
        this.txt = description;
    }

    public String getImgId() {
        return imageId;
    }

    public void setImgId(String imgId) {
        this.imageId = imgId;
    }
}

